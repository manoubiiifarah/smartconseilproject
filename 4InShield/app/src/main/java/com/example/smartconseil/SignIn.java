package com.example.smartconseil;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class SignIn extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        Button signing=findViewById(R.id.signInBtn2);
        signing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            UsefullMethods.goToActivity(SignIn.this,SpaceChose.class);
            }
        });
    }
}